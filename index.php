<?php
session_start();
include_once('../../library/nvn_core.php');
include_once('../../library/nvn_page.php');
include_once('../../library/jdf.php');

check_login();
get_settings();
nvn_navigate("wait");
function wait($req)
{
	echo up_html();
	echo down_html();
}
function install($input)
{
	check_login();
	echo up_html();
	echo "آغاز نصب ماژول...<br />";
	//Configuration
	$params = array(
		"module_name" => "teach",
		"author" => "farzan",
		"version" => "0.1",
		"data" => "",
		"enable" => 1,
		//unComment if exists
		/*$module_functions = array(
			array("type"=>"startup","name"=>"test1","module_name"=>$module_name,"library"=>"","function"=>""),
			array("type"=>"startup","name"=>"test2","module_name"=>$module_name,"library"=>"","function"=>""),
		);*/
		"table_queries" => array(""),
	);
	module_install($params);
}
function add_name()
{
	$echo="
		<table dir=rtl>
		<form id='report' method='POST' action=''  enctype='multipart/form-data'>
			<tr>
				<td>
					IP :
				</td>
				<td>
					<input type='text'   name='ip' value='192.168.16.254'/>
				</td>
			</tr>
			<tr>
				<td>
					PORT :
				</td>
				<td>
					<input type='text'   name='port' value='8080'/>
				</td>
			</tr>
			<tr>
				<td>
					نام :
				</td>
				<td>
					<input type='text'   name='string' maxlength='20'/>
				</td>
			</tr>
			<tr>
				<td>
					شماره کاربر :
				</td>
				<td>
					<input type='text'   name='member_id' />
				</td>
			</tr>
			<tr>
				<td colspan ='2'>
					<input type='submit' value='تایید و ارسال به دستگاه' >
					<input type='hidden' name='action' value='convert_to_print'>
				</td>
			</tr>
		</form>
		</table>
		<a href='index.php?action=send_all&ip=192.168.16.254&port=8080'>افزودن همه کارمندان</a>
	";
	$params = array("title"=>"افزودن نام کاربرر | ".$GLOBALS['Globalset']['title'],
		"header"=>"افزودن نام کاربر",
		"sub_header"=>$GLOBALS['Globalset']['title'],
		"text"=>$echo,
		"back"=>"".root()."menu.php",
		);
	$page = new nvn_page($params);
	echo $page->show();
}

function send_all($POST)
{
		set_time_limit ( 3000 );

	$cxn=DB_Connect();
	$query="SELECT * FROM `nvnm_sac_workers` ORDER BY card_id Asc";
	$result=mysqli_query($cxn,$query)
	or die("Could'nt connect personnel <br/>".mysqli_error($cxn));
	while ($row = mysqli_fetch_assoc($result))
	{
		@$echoh .= $row['fullname'] . ":";
		$par = array(
						"ip" => $POST['ip'],
						"port" => $POST['port'],
						"return" => "yes",
						"member_id" => $row['card_id'],
						"string" => $row['fullname'],
						
					);
		$echoh .= convert_to_print($par)."<br/>";
		usleep(300000);
	}
	$params = array("title"=>"افزودن نام کاربران | ".$GLOBALS['Globalset']['title'],
		"header"=>"افزودن نام کاربران",
		"sub_header"=>$GLOBALS['Globalset']['title'],
		"text"=>$echoh,
		"back"=>"".root()."menu.php",
		);
	$page = new nvn_page($params);
	echo $page->show();
}

function convert_to_print($string)
{
		set_time_limit ( 3000 );

	$m_id=$string['member_id'];
	$ip=$string['ip'];
	$port=$string['port'];
	$return = $string['return'];
	
	global $echo,$singles,$starter_middles,$starters,$middles,$frees,$endings,$connected_endings,$single_endings;

	//$string = "آقا مهدی معین جان، سلام به شما دوست عزيز و ماه و مهربانم بیا بریم باغ بدیع انسان";
	$string=$string['string'];
	//echo $string;

	//echo "<br/>";
	$array = mbStringToArray ($string);

	$singles = array(
					"0" => "128",
					"1" => "129",
					"2" => "130",
					"3" => "131",
					"4" => "132",
					"5" => "133",
					"6" => "134",
					"7" => "135",
					"8" => "136",
					"9" => "137",
					"،" => "138",
					"," => "138",
					"ـ" => "139",
					"؟" => "140",
					"?" => "140",
					"آ" => "141",
					"ء" => "143",
					" " => "32",
					"" => "",
					//"د" => "162",
					//"ذ" => "163",
					//"ر" => "164",
					//"ز" => "165",
					//"ژ" => "166",
					//"ط" => "175",
					//"ظ" => "176",
					//"و" => "200",
					);
	$starter_middles = array(//Left side Must and Right side can
					"ئ" => "142",//ئـ
					"ب" => "147",//بـ
					"پ" => "149",//پـ
					"ت" => "151",//تـ
					"ث" => "153",//ثـ
					"ج" => "155",//جـ
					"چ" => "157",//چـ
					"ح" => "159",//حـ
					"خ" => "161",//خـ
					"س" => "168",//سـ
					"ش" => "170",//شـ
					"ص" => "172",//صـ
					"ض" => "174",//ضـ
					"ف" => "186",//فـ
					"ق" => "188",//قـ
					"ک" => "190",//کـ
					"گ" => "192",//گـ
					"ل" => "195",//لـ
					"م" => "197",//مـ
					"ن" => "199",//نـ
					"ی" => "206",//یـ
					"ي" => "206",//يـ
					
					
					);
	$frees = array(
					"ط" => "175",
					"ظ" => "176",
					);
					
	$starters = array(//Leftside Must connect
					"ع" => "180",//عـ just starter
					"غ" => "184",//غـ just starter
					"ه" => "203",//هـ just starter
	);				
	$middles = array(//Both sides Must connect
					"ع" => "179",//ـعـ
					"غ" => "183",//ـغـ
					"ه" => "202",//ـهـ
	);
	$endings = array(//Right Side can connect
					"ب" => "146",
					"پ" => "148",
					"ت" => "150",
					"ث" => "152",
					"ج" => "154",
					"چ" => "156",
					"ح" => "158",
					"خ" => "160",
					"س" => "167",
					"ش" => "169",
					"ص" => "171",
					"ض" => "173",
					"ف" => "185",
					"ق" => "187",
					"ک" => "189",
					"گ" => "191",
					"ل" => "193",
					"م" => "196",
					"ن" => "198",
					
					"د" => "162",
					"ذ" => "163",
					"ر" => "164",
					"ز" => "165",
					"ژ" => "166",
					"و" => "200",
					"ه" => "201",

					);
	$connected_endings = array(//Right Side Must Connect
					"ا" => "145",//ـا
					"ع" => "178",//ـع
					"غ" => "182",//ـغ
					"ی" => "204",//ـی
					"ي" => "204",//ـي
					//"ه" => "201",//ـه
					);
	$single_endings = array(//No Side Can Connect
					"ا" => "144",
					"ع" => "177",
					"غ" => "181",
					//"ه" => "201",
					"ی" => "205",
					);
					//			"لا" => "194",





	$echo = "";
	$chars = array();
	$codes = array();
	foreach($array as $key => $value)
	{
		$ct = cct_detect($array,$key);
		if($ct['type']=='exception')
		{
			$array[$key+1] = "";
		}
		$chars[$key] = $ct['char'];
		$codes[$key] = $ct['code'];
	}
	$ne="";
	foreach ($chars as $value)
	{
		$ne = $ne." ".$value;
	}
	$chs = "";
	foreach ($codes as $value)
	{
		$chs = $value." ".$chs;
	}
	
	//echo"<div style='direction:rtl;'>".$ne."</div><br/>";
	//echo"<div style='direction:ltr;'>".$chs."</div><br/>";
	//echo"<a href='index.php'>تست بعدی</a>";
	//echo $echo;
	//print_R($codes);
	$start=4980+(20*$m_id);
	//echo "start=".$start;
	$minus = 0;
	//print_r($chs);
	$key = count($codes);
	while($key<20)
	{
		$code="20";
		$add=strtoupper(dechex($start));
		//write
		$query = "AEBCD81".$add.$code;
		//echo "</br>".$query;
		//$start = microtime(true);
		$client = stream_socket_client("tcp://".$ip.":".$port, $errno, $errorMessage);
		if ($client === false) 
		{
		 $echo .= "error<br />";
		}
		fwrite($client, $query);
		$c = fread($client,10);
		$echo2 .= " z:".$c;
		fclose($client);
		
		$start++;
		$key++;
		//sleep(1);
		 usleep(500000);
	}
	$codes = array_reverse($codes);
	foreach ($codes as $key=> $value)
	{
		//if($key == 20) break;
		if($value=="")
		{
			$minus ++;
			continue;
		}
		$code=strtoupper(dechex($value));
		$add=strtoupper(dechex($start));
		//write
		$query = "AEBCD81".$add.$code;
		//echo "</br>".$query;
		//$start = microtime(true);
		$client = stream_socket_client("tcp://".$ip.":".$port, $errno, $errorMessage);
		if ($client === false) 
		{
		 $echo .= "error<br />";
		}
		fwrite($client, $query);
		$c = fread($client,10);
		$echo2 .= " c:".$c;
		fclose($client);
		$start++;
		//sleep(1);
		usleep(500000);
	}
	//$key = $key + 1 - $minus;
	//echo "<br/>key=>".$key."<br/>";

	
	$echo .= "ثبت با موفقیت انجام شد";
	$echo .= "<br/>";
	if($return == 'yes')
	{
		return $echo2;
	}else
	{
		$params = array("title"=>"افزودن نام کاربر | ".$GLOBALS['Globalset']['title'],
			"header"=>"افزودن نام کاربر",
			"sub_header"=>$GLOBALS['Globalset']['title'],
			"text"=>$echo."<br/>".$echo2,
			"back"=>"".root()."menu.php",
			);
		$page = new nvn_page($params);
		echo $page->show();
	}
	
}
function cct_detect($array,$current)//current char type
{
	global $singles,$starter_middles,$starters,$middles,$frees,$endings,$connected_endings,$single_endings;
	$echo .= $current.":<br/>";

	if($array[$current] == 'ل' and $array[$current+1] == 'ا')
	{
		$echo .= "test0.0: current:".$current." current char: لا code: 194 <br/>";
		return array('type' => 'exception', 'code' => '194', "char" => 'لا');
	}else if($current == 0 or (! lc_check($array[$current-1])))
	{
		$echo .= "test0.1: current:".$current." current char: ".$array[$current]." Starter <br/>";
		if(isset($singles[$array[$current]]))
		{
			$echo .= "test1: current:".$current." current char: ".$array[$current]." code: ".$singles[$array[$current]].'<br/>';
			return array('type' => 'single', 'code' => $singles[$array[$current]], "char" => $array[$current]);
		}else if(lc_check($array[$current]) and rc_check(@$array[$current+1]))
		{
			$echo .= "test1.5: current:".$current." current char: ".$array[$current]." Left connected Starter<br/>";
			if(isset($starter_middles[$array[$current]]))
			{
				$echo .= "test1.75: current:".$current." current char: ".$array[$current]."ـ code: ".$starter_middles[$array[$current]].'<br/>';
				return array('type' => 'starter_middle', 'code' => $starter_middles[$array[$current]], 'char' => $array[$current]."ـ");
			}else if(isset($starters[$array[$current]]))
			{
				$echo .= "test1.75: current:".$current." current char: ".$array[$current]."ـ code: ".$starters[$array[$current]].'<br/>';
				return array('type' => 'starter', 'code' => $starters[$array[$current]], 'char' => $array[$current]."ـ");
			}else if(isset($frees[$array[$current]]))
			{
				$echo .= "test1.75: current:".$current." current char: ".$array[$current]."ـ code: ".$frees[$array[$current]].'<br/>';
				return array('type' => 'starter_middle', 'code' => $frees[$array[$current]], 'char' => $array[$current]."ـ");
			}			
		}else if(! @rc_check($array[$current+1]) or !lc_check($array[$current]))
		{
			$echo .= "test6: current:".$current." current char: ".$array[$current]." Ending Starter<br/>";
			if(isset($single_endings[$array[$current]]))
			{
				$echo .= "test6.75: current:".$current." current char: ".$array[$current]." code: ".$single_endings[$array[$current]].'<br/>';
				return array('type' => 'starter_middle', 'code' => $single_endings[$array[$current]], 'char' => $array[$current]);
			}else if(isset($endings[$array[$current]]))
			{
				$echo .= "test6.75: current:".$current." current char: ".$array[$current]." code: ".$endings[$array[$current]].'<br/>';
				return array('type' => 'starter', 'code' => $endings[$array[$current]], 'char' => $array[$current]);
			}

		}
	}else
	{
		//$echo .= "cont".$array[$current];
		if(isset($singles[$array[$current]]))
		{
			$echo .= "test3: current:".$current." current char: ".$array[$current]." code: ".$singles[$array[$current]].'<br/>';
			return array('type' => 'single', 'code' => $singles[$array[$current]], 'char' => $array[$current]);
		}else if(lc_check($array[$current-1]) and rc_check($array[$current]) and lc_check($array[$current]) and rc_check(@$array[$current+1]))
		{
			$echo .= "test4: current:".$current." current char: ".$array[$current]." Middle Connected<br/>";
			if(isset($starter_middles[$array[$current]]))
			{
				$echo .= "test4.75: current:".$current." current char: ـ".$array[$current]."ـ code: ".$starter_middles[$array[$current]].'<br/>';
				return array('type' => 'starter_middle', 'code' => $starter_middles[$array[$current]], 'char' => "ـ".$array[$current]."ـ");
			}else if(isset($middles[$array[$current]]))
			{
				$echo .= "test4.75: current:".$current." current char: ـ".$array[$current]."ـ code: ".$middles[$array[$current]].'<br/>';
				return array('type' => 'starter', 'code' => $middles[$array[$current]], 'char' => "ـ".$array[$current]."ـ");
			}else if(isset($frees[$array[$current]]))
			{
				$echo .= "test4.75: current:".$current." current char: ـ".$array[$current]."ـ code: ".$frees[$array[$current]].'<br/>';
				return array('type' => 'starter_middle', 'code' => $frees[$array[$current]], 'char' => "ـ".$array[$current]."ـ");
			}
		
		}else if(lc_check($array[$current-1]) and rc_check($array[$current]) and ( !lc_check($array[$current]) or !rc_check(@$array[$current+1])))
		{
			$echo .= "test5: current:".$current." current char: ".$array[$current]." Right Connected Ending<br/>";
			if(isset($connected_endings[$array[$current]]))
			{
				$echo .= "test5.75: current:".$current." current char: ـ".$array[$current]." code: ".$connected_endings[$array[$current]].'<br/>';
				return array('type' => 'starter_middle', 'code' => $connected_endings[$array[$current]], 'char' => "ـ".$array[$current]);
			}else if(isset($endings[$array[$current]]))
			{
				$echo .= "test5.75: current:".$current." current char: ـ".$array[$current]." code: ".$endings[$array[$current]].'<br/>';
				return array('type' => 'starter', 'code' => $endings[$array[$current]], 'char' => "ـ".$array[$current]);
			}else if(isset($frees[$array[$current]]))
			{
				$echo .= "test5.75: current:".$current." current char: ـ".$array[$current]." code: ".$frees[$array[$current]].'<br/>';
				return array('type' => 'starter_middle', 'code' => $frees[$array[$current]], 'char' => "ـ".$array[$current]);
			}
		
		}else 
		{
			return array('type' => 'unknown', 'code' => -1, 'char' => -1);
		}
	}
}
function lc_check($char)
{
	global $singles,$starter_middles,$starters,$middles,$frees,$endings,$connected_endings,$single_endings;
	if(isset($starter_middles[$char]))
	{
		return 1;
	}else if(isset($starters[$char]))
	{
		return 1;
	}else if(isset($frees[$char]))
	{
		return 1;
	}else if(isset($middles[$char]))
	{
		return 1;
	}else
	{
		return 0;
	}
}
function rc_check($char)
{
	global $singles,$starter_middles,$starters,$middles,$frees,$endings,$connected_endings,$single_endings;
	if(isset($starter_middles[$char]))
	{
		return 1;
	}else if(isset($frees[$char]))
	{
		return 1;
	}else if(isset($middles[$char]))
	{
		return 1;
	}else if(isset($endings[$char]))
	{
		return 1;
	}else if(isset($connected_endings[$char]))
	{
		return 1;
	}else
	{
		return 0;
	}
}
function mbStringToArray ($string) {
    $strlen = mb_strlen($string);
    while ($strlen) {
        $array[] = mb_substr($string,0,1,"UTF-8");
        $string = mb_substr($string,1,$strlen,"UTF-8");
        $strlen = mb_strlen($string);
    }
    return $array;
} 





?>